package com.darkzek.bucks

import android.Manifest
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ErrorCallback
import com.budiyev.android.codescanner.ScanMode
import com.google.zxing.Result


class SendBucks : Fragment() {

    private var codeScanner: CodeScanner? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if (hasCameraPermission()) {
            return inflater.inflate(R.layout.fragment_send_bucks, container, false)
        } else {
            return inflater.inflate(R.layout.fragment_error_send_bucks, container, false)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        if (!hasCameraPermission()) {
            return
        }

        val scannerView = view.findViewById<CodeScannerView>(R.id.scanner_view)
        val activity = requireActivity()
        codeScanner = CodeScanner(context!!, scannerView)
        codeScanner!!.decodeCallback = DecodeCallback {
            handleResult(it)
        }
        scannerView.setOnClickListener {
            codeScanner!!.startPreview()
        }
    }

    override fun onResume() {
        super.onResume()
        if (codeScanner != null) {
            codeScanner!!.startPreview()
        }
    }

    override fun onPause() {
        super.onPause()
        if (codeScanner != null) {
            codeScanner!!.releaseResources()
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            askCameraPermission()
        }
    }

    private fun hasCameraPermission(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }

        if (context!!.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        return false
    }

    private fun askCameraPermission() {

        if (hasCameraPermission()) {
            return
        }

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.CAMERA)) {
            showRationale()
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), 1)
        }
    }

    private fun showRationale() {
        val context = context
        MaterialDialog.Builder(context!!)
                .title(R.string.camera_permission_title)
                .content(R.string.camera_permission_content)
                .onPositive { _, _ -> requestPermissions(arrayOf(Manifest.permission.CAMERA), 1) }.positiveText(R.string.camera_permission_positive_text)
                .onNegative { _, _ -> (activity!!.findViewById<View>(R.id.pager) as ViewPager).currentItem = 1 }.negativeText(R.string.camera_permission_negative_text)
                .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode != 1) {
            return
        }
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Restart fragment
            fragmentManager!!.beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit()
        }
    }

    private fun handleResult(rawResult: Result) {

        if (activity!!.findViewById<ViewPager>(R.id.pager).currentItem != 2) {
            return
        }

        val data = rawResult.text

        val intent = Intent(this.context, SendBucksActivity::class.java)
        intent.putExtra(Intent.EXTRA_TEXT, data)
        startActivity(intent)

        //Pause
        codeScanner!!.releaseResources()
    }
}