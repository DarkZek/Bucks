package com.darkzek.bucks

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

import java.util.ArrayList

import com.darkzek.bucks.Model.Transaction
import com.darkzek.bucks.Model.User
import com.darkzek.bucks.Web.DelayedAPIRequest
import com.darkzek.bucks.Web.Users

class TransactionsAdapter (private val mDataset: ArrayList<Transaction>, var context: Context, var activity: Activity) : RecyclerView.Adapter<TransactionsAdapter.MyViewHolder>() {

    class MyViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var amountText: TextView
        var userText: TextView
        var profile: ImageView

        init {
            amountText = v.findViewById(R.id.transaction_amount)
            userText = v.findViewById(R.id.transaction_user)
            profile = v.findViewById(R.id.transaction_profile)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionsAdapter.MyViewHolder {
        // create a new view
        val v = LayoutInflater.from(parent.context).inflate(R.layout.transaction_row, parent, false) as LinearLayout

        return MyViewHolder(v)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val transaction = mDataset[position]

        Users.getUser(transaction.userId, object: DelayedAPIRequest {
            override fun onRequestCompleted(data: Any) {
                if (data is Boolean) {
                    Log.i("Bucks", "Failed getting user " + transaction.userId)
                    return
                }

                val user = data as User

                holder.userText.text = user.display_name

                user.getProfile(object: DelayedAPIRequest{
                    override fun onRequestCompleted(data: Any) {
                        activity.runOnUiThread { holder.profile.setImageBitmap(data as Bitmap) }
                    }

                })
            }
        })

        holder.amountText.text = "$" + transaction.amount + ""
    }

    override fun getItemCount(): Int {
        return mDataset.size
    }
}
