package com.darkzek.bucks

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.app.Activity
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

import com.darkzek.bucks.Model.User
import com.darkzek.bucks.Web.BucksAPIController
import com.darkzek.bucks.Web.DelayedAPIRequest
import com.darkzek.bucks.Web.SelfUser

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SendBucksActivity : Activity() {

    private val bucksAPI = BucksAPIController.getAPI()

    override fun onCreate(savedInstanceState: Bundle?) {

        //Dark Theme check
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (prefs.getBoolean("app_dark_mode", false)) {
            setTheme(R.style.DarkAppTheme)
        } else {
            setTheme(R.style.DarkAppTheme)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_bucks)

        val data = intent.getStringExtra(Intent.EXTRA_TEXT)


        val paymentInformation = data.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        //Make sure it was a valid QR Code
        if (paymentInformation.size != 4 || !paymentInformation[0].equals("BUX", ignoreCase = true) || paymentInformation[1] != "RECEIVE") {
            //Show toast
            Toast.makeText(this.applicationContext, "Invalid QR Code", Toast.LENGTH_LONG).show()
            this.finish()
            return
        }

        //Update info
        val paymentText = findViewById<TextView>(R.id.amount_text)
        paymentText.text = paymentText.text.toString().replace("%AMOUNT%".toRegex(), paymentInformation[3])

        //Load info
        val userCall = bucksAPI.getUser(Integer.parseInt(paymentInformation[2]))
        userCall.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                val user = response.body()

                val userText = findViewById<TextView>(R.id.user_text)
                userText.text = userText.text.toString().replace("%USER%".toRegex(), user!!.display_name)

                // show The Image in a ImageView
                user.getProfile(object: DelayedAPIRequest {

                    override fun onRequestCompleted(data: Any) {
                        if (data == null || data !is Bitmap) {
                            return
                        }

                        this@SendBucksActivity.runOnUiThread { (findViewById<View>(R.id.send_profile) as ImageView).setImageBitmap(data as Bitmap) }
                    }
                })
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                Log.i("Bucks", "Failed getting user " + paymentInformation[2])
            }
        })

        //Set pay button click action
        findViewById<View>(R.id.pay_bux).setOnClickListener { view ->
            SelfUser.PayUser(Integer.parseInt(paymentInformation[2]), Integer.parseInt(paymentInformation[3]), this@SendBucksActivity)
            this@SendBucksActivity.finish()
        }
    }

}
