package com.darkzek.bucks.Web

import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log

class DownloadImageTask(delayedAPIRequest:DelayedAPIRequest): AsyncTask<String, Void, Void>() {
    internal var delayedAPIRequest:DelayedAPIRequest = delayedAPIRequest

    override fun doInBackground(vararg urls: String): Void? {
        val urldisplay = urls[0]
        try
        {
            val `in` = java.net.URL(urldisplay).openStream()
            val `mIcon11` = BitmapFactory.decodeStream(`in`)
            delayedAPIRequest.onRequestCompleted(mIcon11)
        }
        catch (e:Exception) {
            Log.e("Error", e.message)
            e.printStackTrace()
        }

        return null
    }
}