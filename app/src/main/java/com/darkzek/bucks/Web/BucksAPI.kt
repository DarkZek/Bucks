package com.darkzek.bucks.Web

import com.darkzek.bucks.Model.CurrentUser
import com.darkzek.bucks.Model.TextResponse
import com.darkzek.bucks.Model.User
import com.darkzek.bucks.Model.Transaction

import org.json.JSONObject

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface BucksAPI {
    @FormUrlEncoded
    @POST("api/user/get.php")
    fun getUser(@Field("id") id: Int): Call<User>

    @FormUrlEncoded
    @POST("api/account/register.php")
    fun registerUser(@Field("display_name") display_name: String, @Field("email") email: String, @Field("profile_url") profile_url: String, @Field("google_id") google_id: String, @Field("fcm_token") fcm_token: String): Call<CurrentUser>

    @FormUrlEncoded
    @POST("api/transactions/list.php")
    fun getTransactions(@Field("password") password: String, @Field("id") id: Int): Call<List<Transaction>>

    @FormUrlEncoded
    @POST("api/account/update.php")
    fun getUpdate(@Field("password") password: String, @Field("id") id: Int, @Field("fcm_token") fcm_token: String): Call<CurrentUser>

    @FormUrlEncoded
    @POST("api/transactions/pay.php")
    fun payUser(@Field("password") password: String, @Field("id") id: Int, @Field("target_id") target_id: Int, @Field("amount") amount: Int): Call<TextResponse>

    @FormUrlEncoded
    @POST("api/account/rename.php")
    fun changeDisplayName(@Field("password") password: String, @Field("id") id: Int, @Field("display_name") target_id: String): Call<JSONObject>
}
