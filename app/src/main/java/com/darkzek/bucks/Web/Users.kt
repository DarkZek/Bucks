package com.darkzek.bucks.Web

import android.graphics.Bitmap
import android.util.Log

import com.darkzek.bucks.Model.User

import java.util.ArrayList
import java.util.HashMap

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Users {

    companion object {
        private val bucksAPI = BucksAPIController.getAPI()

        //Caching users
        private val cachedUsers = ArrayList<User>()
        private val waitingRequests = HashMap<Int, ArrayList<DelayedAPIRequest>>()


        //Caching profile pictures
        private val cachedProfiles = HashMap<String, Bitmap>()
        private val waitingProfiles = HashMap<String, ArrayList<DelayedAPIRequest>>()


        //Caches users

        @JvmStatic
        fun getUser(id: Int, api: DelayedAPIRequest) {

            //If we're already waiting for a request back for this user
            if (waitingRequests.containsKey(id)) {
                val apis = waitingRequests[id]!!
                apis.add(api)

                waitingRequests.remove(id)
                waitingRequests[id] = apis
                return
            }

            //Check if we've already cached this user
            for (user in cachedUsers) {
                if (user.id == id) {
                    api.onRequestCompleted(user)
                    return
                }
            }

            //Request user from server
            val userCall = bucksAPI.getUser(id)

            userCall.enqueue(object : Callback<User> {
                override fun onResponse(call: Call<User>, response: Response<User>) {
                    if (response.body() == null) {
                        api.onRequestCompleted(false)
                        return
                    }

                    //Bad code, need to fix this
                    try {
                        (response.body() as User).display_name
                    } catch (e: UninitializedPropertyAccessException) {
                        api.onRequestCompleted(false)
                        return
                    }

                    cachedUsers.add(response.body()!!)

                    waitingRequests[id]!!.forEach {
                        it.onRequestCompleted(response.body() as User)
                    }

                    waitingRequests.remove(id)
                }

                override fun onFailure(call: Call<User>, t: Throwable) {
                    api.onRequestCompleted(false)
                    Log.i("Bucks", "Failed to get user $id")
                }
            })

            //Add request to requests list
            val apis = ArrayList<DelayedAPIRequest>()
            apis.add(api)

            waitingRequests[id] = apis
        }

        //Caches users profiles
        @JvmStatic
        fun getUserProfile(url: String, api: DelayedAPIRequest) {

            //If we're already waiting for a request back for this user
            if (waitingProfiles.containsKey(url)) {
                val apis = waitingProfiles.get(url)!!
                apis.add(api)

                waitingProfiles.remove(url)
                waitingProfiles[url] = apis
                return
            }

            //Check if we've already cached this user
            if (cachedProfiles.containsKey(url)) {
                api.onRequestCompleted(cachedProfiles[url] as Any)
                return
            }

            DownloadImageTask( object : DelayedAPIRequest {
                override fun onRequestCompleted(data: Any) {

                    cachedProfiles[url] = data as Bitmap

                    for (api1 in waitingProfiles[url]!!) {
                        api1.onRequestCompleted(data)
                    }

                    waitingProfiles.remove(url)
                }
            }).execute(url)

            //Add request to requests list
            val apis = ArrayList<DelayedAPIRequest>()
            apis.add(api)

            waitingProfiles[url] = apis
        }
    }
}
