package com.darkzek.bucks.Web

interface DelayedAPIRequest {
    fun onRequestCompleted(data: Any)
}
