package com.darkzek.bucks.Web

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi

import com.darkzek.bucks.Model.User
import com.darkzek.bucks.Preferences.Settings


import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class BucksAPIController : Callback<User> {

    @RequiresApi(api = Build.VERSION_CODES.N)
    override fun onResponse(call: Call<User>, response: Response<User>) {
        if (response.isSuccessful) {
            val user = response.body()
            println(user!!.profileUrl)

        } else {
            println(response.message())
        }
    }

    override fun onFailure(call: Call<User>, t: Throwable) {
        t.printStackTrace()
    }

    companion object {

        internal val BASE_URL = Settings.serverUrl

        var api: BucksAPI? = null

        @JvmStatic
        fun start(content: Context) {
            val okHttpClient = OkHttpClient.Builder()
                    .cache(Cache(content.cacheDir, (10 * 1024 * 1024).toLong()))
                    .build()


            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build()

            api = retrofit.create(BucksAPI::class.java)
        }

        @JvmStatic
        fun getAPI(): BucksAPI {
            return api!!
        }
    }
}