package com.darkzek.bucks.Web


import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.Log
import android.widget.Toast

import com.darkzek.bucks.Model.CurrentUser
import com.darkzek.bucks.Model.Transaction
import com.google.firebase.iid.FirebaseInstanceId

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import android.preference.PreferenceManager
import com.darkzek.bucks.Model.TextResponse

class SelfUser {

    companion object {

        private var bucksAPI: BucksAPI? = null

        var updating = false
        var waitingUpdates = ArrayList<DelayedAPIRequest>()

        private fun CheckBucksAPI() {
            if (bucksAPI == null) {
                bucksAPI = BucksAPIController.getAPI()
            }
        }

        @JvmStatic
        fun PayUser(userId: Int, amount: Int, context: Context) {
            CheckBucksAPI()
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val password = preferences.getString("password", "DEFAULT")
            val id = preferences.getInt("id", 0)

            val call = bucksAPI!!.payUser( password, id, userId, amount)

            call.enqueue(object : Callback<TextResponse> {
                @RequiresApi(api = Build.VERSION_CODES.N)
                override fun onResponse(call: Call<TextResponse>, response: Response<TextResponse>) {
                    System.out.println("Dude the message is " + response.body()!!.message)
                    if (!response.body()!!.message.contentEquals("Success")) {
                        Toast.makeText(context, "Error: " + response.body()!!.message, Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(context, "Successfully paid $$amount", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<TextResponse>, t: Throwable) {
                    Toast.makeText(context, "There was an error paying $" + amount + t.message, Toast.LENGTH_LONG).show()
                }
            })
        }

        @JvmStatic
        fun Update(context: Context?, api: DelayedAPIRequest) {

            waitingUpdates.add(api)

            if (updating) {
                //Already updating, add them to the waiting list for the response
                return
            }
            updating = true

            CheckBucksAPI()

            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val password = preferences.getString("password", "DEFAULT")
            val id = preferences.getInt("id", 0)

            Log.i("Bucks", "Sending update requests...")

            val result = FirebaseInstanceId.getInstance().instanceId

            //Get FCM ID (Notifications)
            result.addOnCompleteListener { task ->
                val fcm_id = task.result.token

                //Update account info
                val userCall = bucksAPI!!.getUpdate(password, id, fcm_id)

                //Call server update and provide fcm id
                userCall.enqueue(object : Callback<CurrentUser> {
                    override fun onResponse(call: Call<CurrentUser>, response: Response<CurrentUser>) {

                        //Log any errors from the server
                        if (!response.isSuccessful || !response.body()!!.message.equals("Success", ignoreCase = true)) {
                            var m = ""
                            if (response.isSuccessful) {
                                m = response.body()!!.message!!
                            }

                            Log.i("Bucks", "Received error update response with message: $m")
                            return
                        }

                        //If there's no errors save the new states
                        SaveUpdatedData(response.body()!!, context!!)

                        //Let all hooks know we have updated
                        for (delayedAPI in waitingUpdates) {
                            delayedAPI.onRequestCompleted("Success")
                        }
                        waitingUpdates = ArrayList<DelayedAPIRequest>()
                        updating = false
                        Log.i("Bucks", "Successfully received update request.")
                    }

                    override fun onFailure(call: Call<CurrentUser>, t: Throwable) {
                        LoadDataFromSave(context!!)

                        for (delayedAPI in waitingUpdates) {
                            delayedAPI.onRequestCompleted("Success")
                        }
                        waitingUpdates = ArrayList<DelayedAPIRequest>()
                        updating = false
                        Log.i("Bucks", "Failed to send update request")
                    }
                })
            }


            val transactionCall = bucksAPI!!.getTransactions(password, id)

            //Update transactions log
            //Call server
            transactionCall.enqueue(object : Callback<List<Transaction>> {
                override fun onResponse(call: Call<List<Transaction>>, response: Response<List<Transaction>>) {
                    //Log an errors from the server
                    if (!response.isSuccessful) {
                        Log.i("Bucks", "Received error transactions update response with message: ")
                        return
                    }

                    //If there's no errors save the new states
                    SaveUpdatedTransactions(response.body(), context!!)
                    api.onRequestCompleted("Success")
                    Log.i("Bucks", "Successfully received transactions update request.")
                }

                override fun onFailure(call: Call<List<Transaction>>, t: Throwable) {
                    api.onRequestCompleted("Failure")
                    Log.i("Bucks", "Failed to send transactions update request" + t.message)
                }
            })
        }

        @JvmStatic
        fun Register(displayName: String, email: String, profilePictureURL: String, googleSignInID: String, fcm_token: String, context: Context, api: DelayedAPIRequest) {
            CheckBucksAPI()
            val call = bucksAPI!!.registerUser(displayName, email, profilePictureURL, googleSignInID, fcm_token)

            call.enqueue(object : Callback<CurrentUser> {
                override fun onResponse(call: Call<CurrentUser>, response: Response<CurrentUser>) {
                    //Log an errors from the server
                    if (!response.isSuccessful || !response.body()!!.message.equals("Success", ignoreCase = true)) {
                        var m = ""
                        if (response.isSuccessful) {
                            m = response.body()!!.message!!
                        }

                        Log.i("Bucks", "Received error register response with message: $m")
                        return
                    }

                    //If there's no errors save the new states
                    SaveUpdatedData(response.body()!!, context)
                    Log.i("Bucks", "Successfully received register request.")

                    api.onRequestCompleted("Success")
                }

                override fun onFailure(call: Call<CurrentUser>, t: Throwable) {
                    Log.i("Bucks", "Failed to send register request." + t.localizedMessage)
                }
            })
        }

        @JvmStatic
        internal fun SaveUpdatedData(user: CurrentUser, context: Context) {
            val settings = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = settings.edit()

            editor.putFloat("bucks", user.bucks)
            editor.putString("profile_url", user.profileUrl)
            editor.putString("display_name", user.display_name)
            editor.putString("email", user.email)
            editor.putInt("id", user.id)
            if (user.password != null) {
                editor.putString("password", user.password)
            }

            CurrentUser._instance = user

            editor.commit()
        }

        internal fun LoadDataFromSave(context: Context) {
            val settings = PreferenceManager.getDefaultSharedPreferences(context)

            val user = CurrentUser()

            user.bucks = settings.getFloat("bucks", 0f)
            user.password = settings.getString("password", "")
            user.profileUrl = settings.getString("profile_url", "")
            user.email = settings.getString("email", "")
            user.id = settings.getInt("id", 0)
            user.display_name = settings.getString("display_name", "")

            CurrentUser._instance = user
        }

        internal fun SaveUpdatedTransactions(transactions: List<Transaction>?, context: Context) {

            val settings = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = settings.edit()

            editor.putString("transactions", Transaction.serializeList(transactions!!))
            editor.commit()
        }
    }
}
