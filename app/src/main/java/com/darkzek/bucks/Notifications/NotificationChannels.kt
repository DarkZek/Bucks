package com.darkzek.bucks.Notifications

enum class NotificationChannels private constructor(private val value: String) {
    NEW_PAYMENTS("NEW_PAYMENTS");

    fun value(): String {
        return value
    }
}
