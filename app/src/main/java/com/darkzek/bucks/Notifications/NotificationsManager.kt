package com.darkzek.bucks.Notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat

import java.util.Random

import com.darkzek.bucks.R

class NotificationsManager {

    fun SendNotification(title: String, content: String, channel: NotificationChannels, context: Context) {
        val mBuilder = NotificationCompat.Builder(context, channel.value())
                .setSmallIcon(R.drawable.circle)
                .setContentTitle(title)
                .setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        val notificationManager = NotificationManagerCompat.from(context)

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(Random().nextInt(9999), mBuilder.build())

    }

    fun createNotificationChannels(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            for (c in NotificationChannels.values()) {
                val importance = NotificationManager.IMPORTANCE_DEFAULT

                val channel = NotificationChannel(c.value(), c.value(), importance)
                channel.description = c.value()
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                val notificationManager = context.getSystemService(NotificationManager::class.java)
                notificationManager.createNotificationChannel(channel)
            }
        }
    }

    companion object {
        private val _instance = NotificationsManager()

        fun getInstance(): NotificationsManager {
            return _instance
        }
    }

}
