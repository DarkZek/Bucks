package com.darkzek.bucks.Service

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import com.darkzek.bucks.ReceivedMoneyActivity

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FirebaseService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        // Check if message contains a notification payload.
        if (remoteMessage!!.notification != null) {
            RecievedMoney(remoteMessage.data)
        }
    }

    fun RecievedMoney(description: Map<String, String>) {
        //Vibrate
        VibratePhone()

        val name = description["name"]
        val amount = description["amount"]
        val profileUrl = description["profile_url"]

        val myIntent = Intent(applicationContext, ReceivedMoneyActivity::class.java)
        myIntent.putExtra("name", name)
        myIntent.putExtra("amount", amount)
        myIntent.putExtra("profile_url", profileUrl)
        myIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        applicationContext.startActivity(myIntent)

    }

    private fun VibratePhone() {
        val v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            //deprecated in API 26
            v.vibrate(500)
        }
    }

}
