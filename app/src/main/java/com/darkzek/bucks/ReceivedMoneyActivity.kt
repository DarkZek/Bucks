package com.darkzek.bucks

import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.darkzek.bucks.Web.DelayedAPIRequest

import com.darkzek.bucks.Web.Users

class ReceivedMoneyActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var continueOn: TextView
    private var payer: String? = null
    private var amount: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        payer = intent.getStringExtra("name")
        amount = intent.getStringExtra("amount")

        Log.i("Bucks", "Showing received money popup from " + payer!!)

        //Dark Theme check
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (prefs.getBoolean("app_dark_mode", false)) {
            setTheme(R.style.DarkAppTheme)
        } else {
            setTheme(R.style.DarkAppTheme)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.recieved_money_activity)

        continueOn = findViewById(R.id.continue_button)
        continueOn.setOnClickListener(this)

        (findViewById<View>(R.id.name) as TextView).text = payer
        (findViewById<View>(R.id.amount) as TextView).text = "$" + amount!!

        val profileUrl = intent.getStringExtra("profile_url")

        Users.getUserProfile(profileUrl, object: DelayedAPIRequest {

            override fun onRequestCompleted(data: Any) {
                if (data !is Bitmap) {
                    return
                }

                this@ReceivedMoneyActivity.runOnUiThread { (findViewById<View>(R.id.profile) as ImageView).setImageBitmap(data) }
            }
        })
    }

    override fun onPointerCaptureChanged(hasCapture: Boolean) {

    }

    override fun onClick(view: View) {
        finish()
    }
}
