package com.darkzek.bucks

import android.content.Intent
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.darkzek.bucks.Model.CurrentUser
import com.darkzek.bucks.Notifications.NotificationsManager
import com.darkzek.bucks.Web.BucksAPIController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import android.support.design.widget.BottomNavigationView
import android.preference.PreferenceManager


class MainActivity : AppCompatActivity() {

    //TODO: Take a look at Glide https://github.com/bumptech/glide

    private lateinit var pageCollectionAdapter: PageCollectionAdapter
    private lateinit var mViewPager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {

        CurrentUser.loadDefaultUser(this)
        BucksAPIController.start(this)

        super.onCreate(savedInstanceState)

        if (!PreferenceManager.getDefaultSharedPreferences(this).contains("id") || GoogleSignIn.getLastSignedInAccount(this) == null) {
            setTheme(R.style.AppTheme)
            setContentView(R.layout.activity_sign_up)
            //Sign up
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
            this.finish()
            return
        }

        //Setup notification channels
        NotificationsManager.getInstance().createNotificationChannels(this)

        //Dark Theme check
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (prefs.getBoolean("app_dark_mode", false)) {
            setTheme(R.style.DarkAppTheme)
        } else {
            setTheme(R.style.AppTheme)
        }

        setContentView(R.layout.activity_main)

        pageCollectionAdapter = PageCollectionAdapter(supportFragmentManager)
        mViewPager = findViewById(R.id.pager)
        mViewPager.adapter = pageCollectionAdapter

        //Select default tage
        mViewPager.currentItem = 1
        findViewById<BottomNavigationView>(R.id.bottom_navigation).menu.getItem(1).isChecked = true;


        findViewById<BottomNavigationView>(R.id.bottom_navigation).setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_receive -> mViewPager.currentItem = 0;
                R.id.action_balance -> mViewPager.currentItem = 1;
                R.id.action_send -> mViewPager.currentItem = 2;
            }
            false
        }


        mViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                var nav = findViewById<BottomNavigationView>(R.id.bottom_navigation)
                nav.menu.getItem(0).isChecked = false
                nav.menu.getItem(1).isChecked = false
                nav.menu.getItem(2).isChecked = false

                nav.menu.getItem(position).isChecked = true
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    override fun onBackPressed() {
        if (mViewPager.currentItem != 1) {
            mViewPager.currentItem = 1
        } else {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        mViewPager.setCurrentItem(1, false)
        mViewPager.offscreenPageLimit = 6
    }
}