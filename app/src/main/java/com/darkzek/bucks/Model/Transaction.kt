package com.darkzek.bucks.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList

class Transaction(@field:SerializedName("payer_id")
                  @field:Expose
                  val userId: Int, @field:SerializedName("amount")
                  @field:Expose
                  val amount: Int) {

    fun serialize(): String {
        return "{$userId:$amount}"
    }

    companion object {

        fun unserialize(data: String): Transaction {
            val transaction = data.substring(1, data.length - 1).split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

            if (transaction.size != 2) {
                return Transaction(0, 0)
            }

            try {
                val userId = Integer.parseInt(transaction[0])
                val amount = Integer.parseInt(transaction[1])

                return Transaction(userId, amount)

            } catch (e: NumberFormatException) {
                throw NullPointerException("Invalid data $data")
            }

        }

        fun serializeList(list: List<Transaction>): String {
            if (list.size == 0) {
                return ""
            }
            var message = ""
            for (transaction in list) {
                message += transaction.serialize() + ","
            }
            return message.substring(0, message.length - 1)
        }

        fun unserializeList(data: String?): ArrayList<Transaction>? {

            if (data == null || data === "") {
                return null
            }

            val strings = data.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

            val transactions = ArrayList<Transaction>()

            for (string in strings) {
                try {
                    transactions.add(Transaction.unserialize(string))
                } catch (e: NullPointerException) {

                }

            }

            return transactions
        }
    }
}
