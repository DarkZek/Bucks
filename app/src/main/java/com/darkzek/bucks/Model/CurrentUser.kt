package com.darkzek.bucks.Model

import android.content.Context

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import android.preference.PreferenceManager

class CurrentUser : User() {

    @SerializedName("bux")
    @Expose
    var bucks: Float = 0.toFloat()

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("user_id")
    @Expose
    private val userId: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("password")
    @Expose
    var password: String? = null


    fun getUserId(): Int {
        return 2
    }

    companion object {

        var _instance = CurrentUser()

        fun loadDefaultUser(context: Context) {
            val settings = PreferenceManager.getDefaultSharedPreferences(context)

            _instance.display_name = settings.getString("display_name", "")!!
            _instance.profileUrl = settings.getString("profile_url", "")!!
            _instance.id = settings.getInt("id", 0)
            _instance.email = settings.getString("email", "")
            _instance.bucks = settings.getFloat("bucks", 0f)
        }

        fun getInstance(): CurrentUser {
            return _instance
        }
    }
}
