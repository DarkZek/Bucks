package com.darkzek.bucks.Model

import com.darkzek.bucks.Web.DelayedAPIRequest
import com.darkzek.bucks.Web.Users
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class User {
    @SerializedName("display_name")
    lateinit var display_name: String

    @SerializedName("profile_url")
    lateinit var profileUrl: String

    @SerializedName("id")
    @Expose
    var id: Int = 0

    constructor(display_name: String, profileUrl: String, id: Int) {
        this.display_name = display_name
        this.profileUrl = profileUrl
        this.id = id
    }

    protected constructor() {}

    fun getProfile(api: DelayedAPIRequest) {
        Users.getUserProfile(profileUrl, object: DelayedAPIRequest {
            override fun onRequestCompleted(data: Any) {
                api.onRequestCompleted(data)
            }
        })
    }
}
