package com.darkzek.bucks.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TextResponse {
    @SerializedName("message")
    @Expose
    var message: String = ""
}