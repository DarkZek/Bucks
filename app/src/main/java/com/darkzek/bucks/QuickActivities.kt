package com.darkzek.bucks

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.darkzek.bucks.Model.CurrentUser
import com.darkzek.bucks.Model.Transaction
import com.darkzek.bucks.Web.SelfUser

import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.preference.PreferenceManager
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import com.darkzek.bucks.Preferences.SettingsActivity
import com.darkzek.bucks.Web.DelayedAPIRequest


class QuickActivities : Fragment() {


    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: RecyclerView.Adapter<*>? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null

    override fun onResume() {
        super.onResume()
        UpdateInformation()
    }

    fun UpdateInformation() {
        //Update app
        SelfUser.Update(this.context!!, object : DelayedAPIRequest {
            override fun onRequestCompleted(data: Any) {
                //Load Money
                val currentUser = CurrentUser.getInstance()

                (this@QuickActivities.view!!.findViewById<View>(R.id.bux) as TextView).text = "$" + currentUser.bucks
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_quick_activities, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        SelfUser.Update(this.context, object: DelayedAPIRequest {
            override fun onRequestCompleted(data: Any) {
                showTransactions()
            }
        })

        activity!!.findViewById<ImageView>(R.id.settingsIcon).setOnClickListener {
            var settingsIntent = Intent(context, SettingsActivity::class.java)
            startActivity(settingsIntent)
        }
    }

    private fun showTransactions() {

        val previousTransactions = Transaction.unserializeList(PreferenceManager.getDefaultSharedPreferences(context).getString("transactions", "[]"))

        if (previousTransactions == null || previousTransactions.size == 0) {
            return
        }

        val view = view

        //Setup transactions recycler view
        mRecyclerView = view!!.findViewById(R.id.transactions_recycler_view)
        mRecyclerView!!.setHasFixedSize(true)

        // use a linear layout manager
        mLayoutManager = object : LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        mRecyclerView!!.layoutManager = mLayoutManager

        // specify an adapter (see also next example)
        mAdapter = TransactionsAdapter(previousTransactions, context!!, activity as Activity)
        mRecyclerView!!.adapter = mAdapter
    }
}
