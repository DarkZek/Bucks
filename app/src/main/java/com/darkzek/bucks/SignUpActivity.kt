package com.darkzek.bucks

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.darkzek.bucks.Web.DelayedAPIRequest
import com.darkzek.bucks.Web.SelfUser
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId

class SignUpActivity : Activity() {

    private var signingIn = false

    private lateinit var mGoogleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestProfile()
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build()

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    override fun onResume() {
        super.onResume()

        if (signingIn) {
            return
        }

        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, 9009)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        signingIn = true

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == 9009) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            val result = FirebaseInstanceId.getInstance().instanceId

            result.addOnCompleteListener { task ->
                Log.i("Bucks", "Sending Register Request To Server..")
                val id = task.result.token

                SelfUser.Register(account.displayName!!, account.email!!, account.photoUrl!!.toString(), account.idToken!!, id, this, object: DelayedAPIRequest {
                    override fun onRequestCompleted(data: Any) {
                        val intent = Intent(this@SignUpActivity, MainActivity::class.java)
                        startActivity(intent)
                        this@SignUpActivity.finish()
                    }
                })
            }


        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("TAG", "signInResult:failed code=" + e.statusCode)

        }

    }
}
