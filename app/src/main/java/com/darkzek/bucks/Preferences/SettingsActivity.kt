package com.darkzek.bucks.Preferences

import android.os.Bundle

import com.darkzek.bucks.R
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.preference.*
import android.util.Log
import com.darkzek.bucks.Model.CurrentUser
import com.darkzek.bucks.Web.BucksAPIController
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.widget.Toast
import android.R.attr.data
import android.app.PendingIntent.getActivity


class SettingsActivity : PreferenceActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

    private val bucksAPI = BucksAPIController.getAPI()

    override fun isValidFragment(fragmentName: String?): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    override fun onBuildHeaders(target: List<PreferenceActivity.Header>) {
        loadHeadersFromResource(R.xml.preference_headers, target)
    }

    override fun onApplyThemeResource(theme: Resources.Theme, resid: Int, first: Boolean) {
        //Dark Theme check
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)

        if (prefs.getBoolean("app_dark_mode", false)) {
            theme.applyStyle(R.style.DarkAppTheme, true)
        } else {
            theme.applyStyle(R.style.AppTheme, true)
        }
    }

    override fun onSharedPreferenceChanged(prefs: SharedPreferences?, string: String?) {
        when (string) {
            "display_name" -> updateDisplayName(prefs!!)
        }
    }

    private fun updateDisplayName(prefs: SharedPreferences) {
        bucksAPI.changeDisplayName(prefs.getString("password", ""), prefs.getInt("id", 0), prefs.getString("display_name", "")).enqueue(object : Callback<JSONObject> {
            override fun onResponse(call: Call<JSONObject>, response: Response<JSONObject>) {
                if (!response.body()!!.has("message")) {
                    return
                }

                Toast.makeText(this@SettingsActivity, response.body()!!.getString("message"),
                        Toast.LENGTH_LONG).show()
            }

            override fun onFailure(call: Call<JSONObject>, t: Throwable) {
                Toast.makeText(this@SettingsActivity, resources.getString(R.string.pref_general_user_display_name_error),
                        Toast.LENGTH_LONG).show()
            }
        })
    }

    //
    // Sub fragments
    //

    class GeneralPrefsFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            addPreferencesFromResource(R.xml.pref_general)
        }
    }
}