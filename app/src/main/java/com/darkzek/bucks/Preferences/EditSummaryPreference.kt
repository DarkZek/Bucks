package com.darkzek.bucks.Preferences

import android.content.Context
import android.preference.EditTextPreference
import android.preference.PreferenceManager
import android.util.AttributeSet

class EditSummaryPreference(context: Context?, attrs: AttributeSet?) : EditTextPreference(context, attrs) {

    override fun getSummary(): CharSequence {
        if (text == null) {
            return PreferenceManager.getDefaultSharedPreferences(context).getString(key, "Change the name displayed when your profile is shown")!!
        }
        return text
    }
}