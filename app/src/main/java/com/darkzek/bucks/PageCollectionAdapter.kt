package com.darkzek.bucks

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class PageCollectionAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getCount(): Int {
        return 3
    }

    override fun getItem(i: Int): Fragment {
        when (i) {
            0 -> {
                return ReceiveBucks()
            }
            1 -> {
                return QuickActivities()
            }
            2 -> {
                return SendBucks()
            }
        }
        return ReceiveBucks()
    }

    override fun getPageTitle(position: Int): CharSequence {
        return "OBJECT " + (position + 1)
    }
}
