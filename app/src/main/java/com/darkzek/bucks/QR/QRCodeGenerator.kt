package com.darkzek.bucks.QR

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.preference.PreferenceManager

import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter

object QRCodeGenerator {

    fun GenerateRecieveQR(userId: Int, amount: Int, backgroundColor: Int, qrColor: Int, width: Int, height: Int): Bitmap? {
        val message = "BUX:RECEIVE:$userId:$amount"
        val writer = QRCodeWriter()
        try {
            val bitMatrix = writer.encode(message, BarcodeFormat.QR_CODE, width, height)
            val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            for (x in 0 until width) {
                for (y in 0 until height) {
                    bmp.setPixel(x, y, if (bitMatrix.get(x, y)) qrColor else backgroundColor)
                }
            }

            return bmp

        } catch (e: WriterException) {
            e.printStackTrace()
        }

        return null
    }

    fun GenerateRecieveQR(userId: Int, amount: Int, context: Context): Bitmap? {
        var qrColor = Color.BLACK
        var backgroundColor = Color.WHITE

        if (backgroundColor == null) {
            backgroundColor = Color.WHITE
        }

        //Adjust to light mode
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        if (prefs.getBoolean("app_dark_mode", false)) {
            qrColor = Color.WHITE
            backgroundColor = Color.TRANSPARENT
        }

        return QRCodeGenerator.GenerateRecieveQR(userId, amount, backgroundColor, qrColor, 512, 512)
    }

}
