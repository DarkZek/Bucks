package com.darkzek.bucks.QR

import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.util.Log
import android.view.TextureView

import java.io.IOException

class CameraModule : TextureView.SurfaceTextureListener {
    private var mCamera: Camera? = null

    private val camera: Camera?
        get() {
            var cam: Camera? = null

            try {
                cam = Camera.open()
            } catch (e: RuntimeException) {
                Log.i("Camera not available", "camera ded")
            }

            return cam
        }

    override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
        mCamera = camera

        try {
            mCamera!!.setDisplayOrientation(90)
            mCamera!!.setPreviewTexture(surface)
            mCamera!!.startPreview()

        } catch (ioe: IOException) {
            // Something bad happened
        }

    }

    override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture, width: Int, height: Int) {
        // Ignored, Camera does all the work for us
    }

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
        mCamera!!.stopPreview()
        mCamera!!.release()
        return true
    }

    override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {
        // Invoked every time there's a new Camera preview frame
    }
}
