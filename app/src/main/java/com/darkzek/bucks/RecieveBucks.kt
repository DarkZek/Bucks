package com.darkzek.bucks

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

import com.afollestad.materialdialogs.MaterialDialog

import com.darkzek.bucks.Model.CurrentUser
import com.darkzek.bucks.QR.QRCodeGenerator
import android.content.Intent
import android.net.Uri
import android.graphics.Bitmap.CompressFormat
import android.R.attr.bitmap
import android.os.Environment
import java.io.FileOutputStream


class ReceiveBucks : Fragment(), TextView.OnEditorActionListener, View.OnLongClickListener {

    fun updateQRCode() {
        //Get amount of money requesting
        val moneyAmount = (view!!.findViewById<View>(R.id.amount_to_pay) as EditText).text.toString()
        if (moneyAmount === "") {
            return
        }
        val amount = Integer.parseInt(moneyAmount)

        //Load QR Code
        val qrCode = QRCodeGenerator.GenerateRecieveQR(CurrentUser.getInstance().id, amount, context!!)
        (view!!.findViewById<View>(R.id.qr_code) as ImageView).setImageBitmap(qrCode)

        view!!.findViewById<View>(R.id.qr_code).setOnLongClickListener(this)
    }

    override fun onEditorAction(textView: TextView, i: Int, keyEvent: KeyEvent): Boolean {
        if (i == EditorInfo.IME_ACTION_DONE) {
            updateQRCode()
        }
        return false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_receive_bucks, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceBundle: Bundle?) {
        (getView()!!.findViewById<View>(R.id.amount_to_pay) as EditText).setOnEditorActionListener(this)
        updateQRCode()
    }

    override fun onLongClick(p0: View?): Boolean {

        askStoragePermission()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context!!.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || context!!.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }

        val moneyAmount = (view!!.findViewById<View>(R.id.amount_to_pay) as EditText).text.toString()
        if (moneyAmount === "") {
            return true
        }
        val amount = Integer.parseInt(moneyAmount)

        val map = QRCodeGenerator.GenerateRecieveQR(CurrentUser.getInstance().id, amount, Color.WHITE, Color.BLACK, 64, 64)
        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).absolutePath + "/bucks_qr.png";

        val stream = FileOutputStream(path)
        /* Write bitmap to file using JPEG or PNG and 80% quality hint for JPEG. */
        map!!.compress(CompressFormat.PNG, 80, stream)
        stream.close()

        //Toast.makeText(context, "Saved QR code to gallery", Toast.LENGTH_LONG).show()

        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path))
        sendIntent.type = "image/bmp"
        context!!.startActivity(sendIntent)

        return true
    }

    private fun askStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return
        }

        if (context!!.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && context!!.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return
        }

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            showRationale()
        } else {
            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        }
    }

    private fun showRationale() {
        val context = context
        MaterialDialog.Builder(context!!)
                .title(R.string.storage_permission_title)
                .content(R.string.storage_permission_content)
                .onPositive { _, _ -> requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), 1) }.positiveText(R.string.storage_permission_positive_text)
                .negativeText(R.string.storage_permission_negative_text)
                .show()
    }
}
